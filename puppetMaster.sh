#!/bin/bash

sudo echo "Connexion à l'instance MASTER réussie. Installation de puppet-server"
sleep 3
sudo echo "Installation de puppet serveur en cours"
sudo apt update -y
sudo apt install -y wget
sudo wget https://apt.puppet.com/puppet-release-focal.deb
sudo dpkg -i puppet-release-focal.deb
sudo apt update -y
sudo apt install puppetserver -y
sudo echo 'puppet=/opt/puppetlabs/bin//' >> ~/.bashrc
sudo echo 'PATH=$PATH:$puppet' >> ~/.bashrc
sudo source ~/.bashrc
sudo puppetserver -v
sudo systemctl start puppetserver
sudo systemctl enable puppetserver
exit
# déconnexion
